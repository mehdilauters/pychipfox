import uuid
import json
import requests
from datetime import datetime
import threading
import time
import logging

class Tracker(threading.Thread):
    def __init__(self, chipfox, tracker_id, initial_data=None):
        threading.Thread.__init__(self)
        self.daemon = True
        self.chipfox = chipfox
        self.tracker_id = tracker_id
        self.last_seen = None
        self.position = None
        self.update_callback = None
        
        if initial_data is not None:
            self.last_seen = datetime.fromtimestamp(int(initial_data['lastseen']))
            self.position = float(initial_data['lat']), float(initial_data['lng'])
        
        self.data = initial_data
        self.start()
        
    
    def register_update_callback(self, cb):
        self.update_callback = cb
    
    def fetch_locations(self, max_records=100, next_record=0):
        resp = self.chipfox.post('trackerdata', device_id=self.tracker_id, next=next_record, limit=max_records, language='en')
        if resp['status'] != 200:
            raise Exception(resp)
        else:
            records = resp['response']['response']
            return records
        
    def fetch_all_locations(self):
        all = []
        remaining_data = True
        next_start = 0
        while remaining_data:
            locations = self.fetch_locations(next_record=next_start)
            if not locations['paging']:
                remaining_data = False
            else:
                next_start = locations['paging']['next']

            records = locations['data']
            all += records
        return all
    
    def name(self):
        if self.data is not None:
            return self.data['name']
        return 'Unnamed'

    
    def poll(self):
        record = self.fetch_locations(max_records=1)['data'][0]
        dte = datetime.fromtimestamp(int(record['time']))
        diff = (dte - self.last_seen).total_seconds()
        if diff != 0:
            self.last_seen = dte
            self.position = record['computedLocation']['lat'], record['computedLocation']['lng']
            if self.update_callback is not None:
                self.update_callback(record)
        else:
            pass
    
    def run(self):
        while True:
            try:
                self.poll()
            except Exception as e:
                logging.error('Polling excetpion %s'%e)

            time.sleep(60)
    
    def __repr__(self):
        return f"{self.name()} ({self.tracker_id}): last seen {self.last_seen} @ {self.position}"

class Chipfox:
    URI = 'https://app.chipfox.io/app/'
    #URI = "http://localhost:8989"

    def __init__(self, username, password):
        self.known_trackers = dict()
        self.uuid = '%s'%uuid.uuid4()
        self.session = requests.Session()
        self.initialize_session()

        self.username = username
        self.sign_in(self.username, password)

    def initialize_session(self):
        self.session.cookies.set("UUID", self.uuid)
        self.session.headers.update({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'UUID': self.uuid,
        })

    def sign_in(self, username, password):
        resp = self.post('signin', username=username, password=password, language='en')
        if resp['status'] != 200:
            raise Exception(resp)
        else:
            return True

    def sign_out(self):
        resp = self.post('logout', language='en')
        if resp['status'] != 200:
            raise Exception(resp)
        else:
            return True

    def fetch_trackers_list(self):

        resp = self.post('trackers', request='list', language='en')
        if resp['status'] != 200:
            raise Exception(resp)
        else:
            trackers = resp['response']
            self.known_trackers = {}
            for tracker in trackers:
                self.known_trackers[tracker["id"]] = Tracker(self, tracker["id"], tracker)

        return self.known_trackers

    def post(self, r, **kwargs):
        kwargs['type']=r
        data = kwargs
        try:
            resp = self.session.post(self.URI, data=data)
            try:
                data = json.loads(resp.content)
            except json.decoder.JSONDecodeError as e:
                print('Response: ',resp.content)
                raise e
            return data
        except Exception as e:
            print(e)
            raise e

