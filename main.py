#!/usr/bin/env python3
from chipfox import Chipfox
from datetime import datetime
import sys
import time
import argparse


#pip install gpxpy
import gpxpy
import gpxpy.gpx


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user", help="chipfox api user")
    parser.add_argument("-p", "--password", help="chipfox api password")
    parser.add_argument("-o", "--output", help="output gpx file")
    parser.add_argument("-w", "--wait", action="store_true", help="wait for updates")
    parser.add_argument("-t", "--timestamps", help="filter output between 2 timestamps")
    return parser.parse_args()


def main(args):
    if args.output is not None:
        gpx = gpxpy.gpx.GPX()
        # Create first track in our GPX:
        gpx_track = gpxpy.gpx.GPXTrack()
        gpx.tracks.append(gpx_track)
        # Create first segment in our GPX track:
        gpx_segment = gpxpy.gpx.GPXTrackSegment()
        gpx_track.segments.append(gpx_segment)


    ch = Chipfox(args.user, args.password)

    trackers = ch.fetch_trackers_list()

    def cb(record):
        print('UPDATE: ', record)

    if args.timestamps is not None:
        dfrom, dto = args.timestamps.split(':')
    for _, tracker in trackers.items():
        print(f'{tracker}')
        tracker.register_update_callback(cb)
        records = tracker.fetch_all_locations()
        print(f"    Retrieved a total of {len(records)}")
        for record in records:
            dte = datetime.fromtimestamp(record['time'])
            
            if args.timestamps is not None:
                if int(record['time']) < int(dfrom) or int(record['time']) > int(dto):
                    continue
            
            
            if args.output is not None:
                gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(record['computedLocation']['lat'], record['computedLocation']['lng']))

    if args.output is not None:
        print('Generating %s'%args.output)
        with open(args.output,'w') as f:
            f.write(gpx.to_xml())

    if args.wait:
        print('Waiting for updates. CTRL+C to exit')
        while True:
            time.sleep(10)
        
    ch.sign_out()

if __name__ == '__main__':
    args = parse_args()
    main(args)
